"use strict";

document.addEventListener('DOMContentLoaded', init);

function init() {
    document.querySelector('form').addEventListener('submit', processForm);
    //document.querySelector('#appointmentForm').addEventListener('submit', processAppointment);
}

function processForm(e) {
    e.preventDefault();

    document.querySelector('form').style.visibility = 'hidden';
    document.querySelector('.response').style.visibility = 'visible';
}

